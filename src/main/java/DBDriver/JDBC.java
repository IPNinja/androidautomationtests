package DBDriver;

import libs.ReportWriter;
import org.apache.commons.dbutils.DbUtils;

import java.sql.*;

import static libs.ConfigData.getCfgValue;

public class JDBC {

    private Connection connection;

    private Statement statement;

    public Connection getConnection() {
        return connection;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }


    //////////////////////// PROD DB IN PARTNERS ////////////////////////////

    /**
     * Settings connect to prod db in partners base
     */
    public String getConnectionUrlDb() {
        return "jdbc:mysql://" +
                getCfgValue("MySQL_HOST_DB") + ":" +
                getCfgValue("MySQL_PORT_DB") + "/" +
                getCfgValue("MySQL_NAME_DB") +
                "?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=round";
    }

    /**
     * get prod user login
     */
    public String getUserDb() {
        return getCfgValue("MySQL_USER_DB");
    }

    /**
     * get prod user pass
     */
    public String getPassDb() {
        return getCfgValue("MySQL_PASS_DB");
    }


    //////////////////////// DEV DB IN PARTNERS ///////////////////////

    /**
     * Settings connect to alpha db in partners base
     */
    public String getConnectionUrlDevDb() {
        return "jdbc:mysql://" +
                getCfgValue("MySQL_HOST_DEV_DB") + ":" +
                getCfgValue("MySQL_PORT_DEV_DB") + "/" +
                getCfgValue("MySQL_NAME_DEV_DB");
    }

    /**
     * get alpha user login
     */
    public String getUserDevDb() {
        return getCfgValue("MySQL_USER_DEV_DB");
    }

    /**
     * get alpha user pass
     */
    public String getPassDevDb() {
        return getCfgValue("MySQL_PASS_DEV_DB");
    }


    //////////////////////// PROD DB IN TEST ///////////////////////

    /**
     * Settings connect to prod TEST db in partners base
     */
    public String getConnectionUrlTestDb() {
        return "jdbc:mysql://" +
                getCfgValue("MySQL_HOST_DB") + ":" +
                getCfgValue("MySQL_PORT_DB") + "/" +
                getCfgValue("MySQL_NAME_TEST_DB") +
                "?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=round";
    }

    /**
     * get prod TEST user login
     */
    public String getUserTestDb() {
        return getCfgValue("MySQL_USER_TEST_DB");
    }

    /**
     * get prod TEST user pass
     */
    public String getPassTestDb() {
        return getCfgValue("MySQL_PASS_TEST_DB");
    }

    ////////////////////////// Methods for SQL query //////////////////////////////////

    public String getDataDb(String param, String... args) {
        String value = null;
        String columnName = "id";
        if (args.length > 0) columnName = args[0];
        try (ResultSet resultSet = getStatement().executeQuery(param)) {
            while (resultSet.next()) {
                value = resultSet.getString(columnName);
                break;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return value;
    }

    public String getDataEntity(String query) {
        String value = null;
        try (ResultSet resultSet = getStatement().executeQuery(query)) {
            while (resultSet.next()) {
                value = resultSet.getString("data");
                break;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return value;
    }

    public boolean checkIsHaveEntity(String query) {
        boolean flag = false;
        try (ResultSet resultSet = getStatement().executeQuery(query)) {
            if (!resultSet.first()) {
                flag = true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return flag;
    }

    public void insertQuery(String query) {
        try {
            getStatement().executeUpdate(query);
        } catch (Exception e) {
            System.out.println("Catch " + e);
        }
    }

    public Statement getConnectionDb(String connectionDb, String userDb, String passDb) {
        try {
            ReportWriter.logTestStep("Establish a connection to DB");
            connection = DriverManager.getConnection(connectionDb, userDb, passDb);
            ReportWriter.logInfo("Connection is established");
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return statement;
    }

    public void closeAllConnections() {
        DbUtils.closeQuietly(getStatement());
        DbUtils.closeQuietly(getConnection());
    }
}
