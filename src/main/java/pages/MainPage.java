package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import libs.ReportWriter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.asserts.SoftAssert;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;


public class MainPage extends BaseWidget {

    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.Button[1]")
    private WebElement settingButton;
    @FindBy(xpath = "//android.widget.ImageView[@content-desc='agree']")
    private WebElement acceptLicence;
    @FindBy(xpath = "//android.widget.ImageView[@content-desc='no']")
    private WebElement discardLicence;
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView")
    private WebElement browserSelect;
    @FindBy(id = "android:id/button_always")
    private WebElement alwaysOption;

    @FindBy(id = "com.ipninja.exitnodeapp:id/tvContent")
    private WebElement eulaLink;

    @FindBy(id = "android:id/button_once")
    private WebElement justOnceOption;

    @FindBy(id = "com.ipninja.exitnodeapp:id/tvContent")

    private WebElement licenseAgreementPopup;

    public MainPage(RemoteWebDriver driver) {
        super(driver);
    }


//    TouchAction action = new TouchAction((PerformsTouchActions) driver);

    public void clickToSettingsButton() {
        waitForElementToAppear(settingButton);
        settingButton.click();
    }

    public void clickToAcceptLicenceButton() {
        ReportWriter.logTestStep("Click to accept licence button");
        waitForElementToAppear(acceptLicence);
        acceptLicence.click();
    }

    public void clickToDiscardLicenceButton() {
        waitForElementToAppear(discardLicence);
        discardLicence.click();
    }

    private void selectBrowser() {
        if (browserSelect.isEnabled()) {
            waitForElementToAppear(browserSelect);
            browserSelect.click();
        }
    }

    public void chooseAlwaysOption() {
        if (alwaysOption.isEnabled()) {
            waitForElementToAppear(alwaysOption);
            alwaysOption.click();
        }
    }

    private void chooseJustOnceOption() {
        if (justOnceOption.isEnabled()) {
            waitForElementToAppear(justOnceOption);
            justOnceOption.click();
        }
    }

    private void tabToEulaLink() {
        new TouchAction((AppiumDriver) driver)
                .tap(point(220, 240))
                .waitAction(waitOptions(Duration.ofMillis(250))).perform();
//        waitForElementToAppear(eulaLink);
//        eulaLink.click();
    }

    public void checkLicencePresents() {
        ReportWriter.logTestStep("Click to licence link");
        tabToEulaLink();
//        ReportWriter.logTestStep("Select browser");
//        selectBrowser();
//        ReportWriter.logTestStep("Choose 'JUST ONCE' option for browser");
//        chooseJustOnceOption();
//        helperClass.pressHomeButton();
    }

    public void checkLicenseAgeementPresence() {
        ReportWriter.logTestStep("Check the license popup presence");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue($(licenseAgreementPopup).isDisplayed(), "License agreement popup did not exist");
        softAssert.assertAll();
    }
}