package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class SettingPage extends BaseWidget {

    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.Switch")
    WebElement vpnSwitcher;

    public SettingPage(RemoteWebDriver driver) {
        super(driver);
    }

    public void switchCheckox() throws InterruptedException {
        waitForElementToAppear(vpnSwitcher);
        vpnSwitcher.click();
        Thread.sleep(10000);
    }

}
