package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class ChromeBrowserPage extends BaseWidget {

    public ChromeBrowserPage(RemoteWebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.view.ViewPager/android.widget.FrameLayout/android.widget.Button")
    WebElement acceptChromeLicence;

    @FindBy(xpath = "//android.view.View[@content-desc='Agreement with Partner’s End User Client']")
    WebElement licenceHeader;

    public void clickToAcceptChromeLicence() {
        waitForElementToAppear(acceptChromeLicence);
        acceptChromeLicence.click();
    }

    public boolean getLicenceHeader() {
        waitForElementToAppear(licenceHeader);
        return licenceHeader.isDisplayed();
    }
}
