package libs;


import com.codeborne.selenide.Selenide;
import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class CustomTestListener extends TestBase implements ITestListener {

    @Override
    public void onTestStart(ITestResult tr) {
        tr.setStatus(ITestResult.STARTED); //redundant??
        Reporter.setCurrentTestResult(tr);
        StringBuffer testNameWithParameters = new StringBuffer(tr.getMethod().getMethodName());
        Object[] obj = tr.getParameters();
        if (obj.length > 0) {
            testNameWithParameters.append("[");
            for (int i = 0; i < obj.length; i++) {
                testNameWithParameters.append(obj[i].toString() + ", ");
            }
            testNameWithParameters.append("]");
        }
        ReportWriter.logTestCaseName(testNameWithParameters.toString().replace(", ]", "]"));
        ReportWriter.logTestDescription(tr.getMethod().getDescription());
        Reporter.setCurrentTestResult(null);
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailure(ITestResult tr) {
        Object iTestResult = tr.getInstance();
        RemoteWebDriver webDriver = ((TestBase) iTestResult).getDriver();

        Reporter.setCurrentTestResult(tr);
        ReportWriter.logVideoUrl("http://138.128.240.228:4444/video/" + webDriver.getSessionId() + ".mp4");
//        File screenshot = takeScreenShot(tr, webDriver);
//        Reporter.setCurrentTestResult(null);

//        Integer executionId = testRailReportTCResult(tr, ExecutionStatus.FAILED);
//        testRailAttachScreenshot(executionId, screenshot, screenshot.getName());

//        Path content = Paths.get(screenshot.getPath());
//        attachJavaScriptErrors();
//        try {
//            InputStream is = Files.newInputStream(content);
//            Allure.addAttachment("Screenshot", is);
//        } catch (IOException e){
//            e.printStackTrace();
//        }

    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Attachment
    public String attachJavaScriptErrors() {
        String jsErrors = null;
        List<String> errors = Selenide.getJavascriptErrors();
        for (String error : errors) {
            jsErrors += error + "\n";
        }
        return jsErrors;
    }

    private File takeScreenShot(ITestResult testResult, RemoteWebDriver webDriver) {
        File screenshotFile = null;
        try {
            DateFormat dateFormatForFile = new SimpleDateFormat("HH-mm-ss-SSS");
            DateFormat dateFormatForDir = new SimpleDateFormat("yyyy-MM-dd");

            String filename = dateFormatForFile.format(new Date()) + "_" + testResult.getMethod().getMethodName() + ".png";
            String screenshotsDir = System.getProperty("user.dir").replace("\\", "/") + "/target/surefire-reports/html/Screenshots/";
            String destDir = screenshotsDir + dateFormatForDir.format(new Date());
            String destDirForReport = "Screenshots/" + dateFormatForDir.format(new Date());

            int dpr = 1;
            Screenshot screenshotData = new AShot().shootingStrategy(ShootingStrategies.viewportRetina(100, 0, 0, dpr)).takeScreenshot(webDriver);

            final BufferedImage image = screenshotData.getImage();
            String tmpPath = System.getProperty("user.dir").replace("\\", "/");
            ImageIO.write(image, "PNG", new File(tmpPath + "/" + filename));
            File screenshot = new File(tmpPath + "/" + filename);

            screenshotFile = new File(destDir + "/" + filename);
            FileUtils.moveFile(screenshot, screenshotFile);

            ReportWriter.logScreenShot(destDirForReport + "/" + filename);

        } catch (Exception e) {
            ReportWriter.logException("Exception during takeScreenShot. " + e.toString());
        }
        return screenshotFile;
    }

    @Attachment(value = "Video HTML", type = "text/html", fileExtension = ".html")
    private String attachAllureVideo(RemoteWebDriver webDriver) {
        String videoUrlStr = null;
        if (seleniumGridHubURL != null) {
            String port = seleniumGridHubURL.substring(seleniumGridHubURL.length() - 11, seleniumGridHubURL.length() - 7);
            videoUrlStr = new String("http://104.238.214.137:" + port + "/video/" + webDriver.getSessionId() + ".mp4");
            ReportWriter.logVideoUrl(videoUrlStr);

        }
        return "<html><body><video width='100%' height='100%' controls autoplay><source src='"
                + videoUrlStr + "' type='video/mp4'></video></body></html>";
    }

    public void onStart(ITestContext context) {
    }

    public void onFinish(ITestContext context) {
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }
}