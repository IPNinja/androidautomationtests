package libs;

import DBDriver.JDBC;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HelperClass {
    private RemoteWebDriver driver;
    private JDBC db = new JDBC();
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public HelperClass(RemoteWebDriver driver) {
        this.driver = driver;
    }

    public String executeSelectQuery(List<String> fieldToSelectString,
                                     String tableName,
                                     List<String> searchedParams,
                                     String columnName,
                                     boolean isOrdered,
                                     boolean isLimited,
                                     boolean isOrderedAndLimited) {
        ReportWriter.logTestStep("Execute 'SELECT' query to DB table: '" + tableName + "'");
        String columnValue = "";
        ResultSet resultSet;
        try {
            db.setStatement(db.getConnectionDb(db.getConnectionUrlDevDb(), db.getUserDevDb(), db.getPassDevDb()));
            ReportWriter.logTestStep("Get execution result");

            if (isOrdered) {
                resultSet = db.getStatement().executeQuery("SELECT" + " " + listConverter(fieldToSelectString, ",")
                        + " " + "FROM" + " " + tableName + " " + "WHERE" + " " + listConverter(searchedParams, " and ")
                        + " ORDER BY last_update_time_p desc ");
                columnValue = getColumnValue(resultSet, columnName);
                if (resultSet == null)
                    return "null";
            } else if (isLimited) {
                resultSet = db.getStatement().executeQuery("SELECT" + " " + listConverter(fieldToSelectString, ",")
                        + " " + "FROM" + " " + tableName + " " + "WHERE" + " " + listConverter(searchedParams, " and ") + " LIMIT 1");
                columnValue = getColumnValue(resultSet, columnName);
                if (resultSet == null)
                    return "null";
            } else if (isOrderedAndLimited) {
                resultSet = db.getStatement().executeQuery("SELECT" + " " + listConverter(fieldToSelectString, ",")
                        + " " + "FROM" + " " + tableName + " " + "WHERE" + " " + listConverter(searchedParams, " and ")
                        + " ORDER BY last_update_time_p desc LIMIT 1");
                columnValue = getColumnValue(resultSet, columnName);
                if (resultSet == null)
                    return "null";
            } else {
                resultSet = db.getStatement().executeQuery("SELECT" + " " + listConverter(fieldToSelectString, ",")
                        + " " + "FROM" + " " + tableName + " " + "WHERE" + " " + listConverter(searchedParams, " and "));
                columnValue = getColumnValue(resultSet, columnName);
                if (resultSet == null)
                    return "null";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ReportWriter.logTestStep("Selected value: " + columnValue);
        return columnValue;
    }

    private String getColumnValue(ResultSet resultSet, String columnName) {
        String columnValue = "";
        ReportWriter.logTestStep("Get specific value from DB");
        try {
            while (resultSet != null && resultSet.next()) {
                columnValue = resultSet.getString(columnName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return columnValue;
    }

    /**
     * @param searchedParams - parameters for searching
     * @param waitingTime    - waiting time in minutes
     * @return
     */
    public String getAvailable_pFromDb(List<String> searchedParams, long... waitingTime) {
        String columnValue = "";
        String selectQuery = "SELECT available_p FROM exit_node_t WHERE" + " " + listConverter(searchedParams, " and ")
                + " ORDER BY last_update_time_p desc ";
        ResultSet resultSet;
        try {
            db.setStatement(db.getConnectionDb(db.getConnectionUrlDevDb(), db.getUserDevDb(), db.getPassDevDb()));
            ReportWriter.logTestStep("Get execution result");
            resultSet = db.getStatement().executeQuery(selectQuery);
            ReportWriter.logTestStep("Get specific value from DB");
            if (waitingTime.length == 0) {
                columnValue = getColumnValue(resultSet, "available_p");
            } else {
                ReportWriter.logInfo("Get 'availble_p' after disconnecting from exitNode");
                columnValue = getColumnValue(resultSet, "available_p");
                while (columnValue.equals("1")) {
                    db.setStatement(db.getConnectionDb(db.getConnectionUrlDevDb(), db.getUserDevDb(), db.getPassDevDb()));
                    resultSet = db.getStatement().executeQuery(selectQuery);
                    while (resultSet.next()) {
                        columnValue = resultSet.getString("available_p");
                        ReportWriter.logInfo("Check connection to exitNode");
                        if (columnValue.equals("0")) {
                            ReportWriter.logInfo("SN was disconnected from exitNode");
                            return columnValue;
                        }
                        ReportWriter.logTestStep("Wait " + waitingTime[0] + " minutes");
                        Thread.sleep(waitingTime[0] * 60000);
                    }
                }
            }
        } catch (SQLException | InterruptedException e) {
            e.printStackTrace();
        }
        ReportWriter.logTestStep("Selected value: " + columnValue);
        return columnValue;
    }

    public String getCurrentDate() {
        Date date = new Date(System.currentTimeMillis() - 7200 * 1000);
        return dateFormat.format(date);
    }

    /**
     * Convert list to string splitting by splitChar
     *
     * @param list      - list with parameters
     * @param splitChar - the char or string ('and'/'or')
     * @return
     */
    public String listConverter(List<String> list, String splitChar) {
        String resultString = "";
        if (list.size() == 1) {
            resultString = list.get(0);
        } else {
            resultString = list.get(0);
            for (int i = 1; i < list.size(); i++) {
                resultString += (splitChar + list.get(i));
            }
        }
        ReportWriter.logInfo("String that was converted from list: " + resultString);
        return resultString;
    }

    /**
     * Build list from words
     *
     * @param elements
     * @return
     */
    public List<String> biuldStringList(String... elements) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < elements.length; i++) {
            list.add(elements[i]);
        }
        return list;
    }

    public void pressHomeButton() {
        ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.HOME));
    }

    public void closeApp() {
        ((AppiumDriver) driver).closeApp();
    }

    public void pressSwitchAppButton() {
        ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
    }

    public void clearAllAppButton() {
        ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.CLEAR));
    }

}
