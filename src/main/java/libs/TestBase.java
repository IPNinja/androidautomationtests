package libs;

import DBDriver.JDBC;
import com.codeborne.selenide.WebDriverRunner;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;

@Listeners(libs.CustomTestListener.class)
public class TestBase {

    private static String userName = "igor808";
    private static String accessKey = "bFymT22xMsejDYtjk5rC";

    private JDBC db = new JDBC();

    private RemoteWebDriver driver;

    private String affiliateId;
    private String osVersion;
    private String osType;
    private String appPackageName;
    private String appActivity;
    private String appPath;
    private String deviceName;
    protected String seleniumGridHubURL;

    final DesiredCapabilities dc = new DesiredCapabilities();

    public RemoteWebDriver getDriver() {
        return driver;
    }

    public JDBC getDb() {
        return db;
    }

    public String getAffiliateId() {
        return affiliateId;
    }

    @BeforeClass
    public void setupEmulator() {
    }

    @BeforeMethod
    protected void setup() throws Exception {

        db.setStatement(db.getConnectionDb(db.getConnectionUrlDevDb(), db.getUserDevDb(), db.getPassDevDb()));

        getProperties();

        File filePath = new File(System.getProperty("user.dir"));
        File appDir = new File(filePath, "/app");
        File app = new File(appDir, "app-debug.apk");


        final DesiredCapabilities dc = new DesiredCapabilities();

        switch (PropertiesManager.getInstance().getResourceByName("env")) {
            case "remote":
                dc.setCapability("browserName", osType);
                dc.setCapability("version", osVersion);
                dc.setCapability("appPackage", appPackageName);
                dc.setCapability("appActivity", appPackageName + "." + appActivity);
                dc.setCapability("enableVNC", true);
                dc.setCapability("app", appPath);
                dc.setCapability("enableVNC", true);
                dc.setCapability("screenResolution", "640x360x24");
                AppiumDriver appiumDriver = new AppiumDriver<>(URI.create("http://138.128.240.228:4444/wd/hub").toURL(), dc);
                driver = appiumDriver;
//                driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), dc);
                driver.setFileDetector(new LocalFileDetector());
                break;
            case "local":
                dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "6.0");
                dc.setCapability(MobileCapabilityType.DEVICE_NAME, "api_23");
                dc.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
                URL url = new URL("http://127.0.0.1:4723/wd/hub");
                driver = new AndroidDriver(url, dc);
            case "browserstack":
                dc.setCapability("device", deviceName);//"Google Nexus 6"
                dc.setCapability("os_version", osVersion);
                dc.setCapability("app", "bs://07cd3de3da12c42ae29fae9880c0c7b59724016e");
//                dc.setCapability("app", "ipninjaTest");
                dc.setCapability("browserstack.debug", "true");
                dc.setCapability("browserstack.local", "true");
                dc.setCapability("browserstack.localIdentifier", "Test123");

                driver = new AndroidDriver<AndroidElement>(new URL(seleniumGridHubURL), dc);
                ReportWriter.logInfo("BrowserStack session ID: " + driver.getSessionId().toString());
        }

        WebDriverRunner.setWebDriver(driver);
    }

    @AfterMethod
    public void tearDown() {
        db.closeAllConnections();
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    //    @AfterClass
    public void removeApp() {
        runScript("adb uninstall " + appPackageName);
    }

    private void getProperties() {
        seleniumGridHubURL = System.getProperty("seleniumGridHubURL");
        affiliateId = System.getProperty("affiliateId");
        osVersion = System.getProperty("osVersion");
        osType = System.getProperty("osType");
        appPackageName = System.getProperty("appPackage");
        appActivity = System.getProperty("appActivity");
        appPath = System.getProperty("appPath");
        deviceName = System.getProperty("deviceName");

        if (seleniumGridHubURL == null || seleniumGridHubURL.equals("")) {
//            seleniumGridHubURL = "http://138.128.240.228:4444/wd/hub";
            seleniumGridHubURL = "https://" + userName + ":" + accessKey +    //for BrowserStack
                    "@hub-cloud.browserstack.com/wd/hub";
        }

        if (affiliateId == null || affiliateId.equals("")) {
            affiliateId = "test_27_02_19";
        }
        if (osVersion == null || osVersion.equals("")) {
            osVersion = "6.0";
        }
        if (osType == null || osType.equals("")) {
            osType = "android";
        }
        if (appPackageName == null || appPackageName.equals("")) {
            appPackageName = "com.ipninja.exitnodeapp";
        }
        if (appActivity == null || appActivity.equals("")) {
            appActivity = "MainActivity";
        }
        if (appPath == null || appPath.equals("")) {
            appPath = "http://jenk.ipninja.io/app-paramsFlavor-debug.apk";
        }

        if (deviceName == null || deviceName.equals("")) {
            deviceName = "Google Nexus 6";
        }

        ReportWriter.logInfo("---Parameters---");
        ReportWriter.logInfo("Selenium grid URL: '" + seleniumGridHubURL + "'");
        ReportWriter.logInfo("Affiliate ID: '" + affiliateId + "'");
        ReportWriter.logInfo("OS TYPE: '" + osType + "'");
        ReportWriter.logInfo("OS version: '" + osVersion + "'");
        ReportWriter.logInfo("Device name: '" + deviceName + "'");
        ReportWriter.logInfo("Application package name: '" + appPackageName + "'");
        ReportWriter.logInfo("Application activity: '" + appActivity + "'");
        ReportWriter.logInfo("Path to application: '" + appPath + "'");
        ReportWriter.logInfo("---Parameters---");
    }

    private void runScript(String command) {
        CommandLine oCmdLine = CommandLine.parse(command);
        DefaultExecutor oDefaultExecutor = new DefaultExecutor();
        oDefaultExecutor.setExitValue(0);
        try {
            int iExitValue = oDefaultExecutor.execute(oCmdLine);
        } catch (ExecuteException e) {
            ReportWriter.logError("Execution failed.");
            e.printStackTrace();
        } catch (IOException e) {
            ReportWriter.logError("permission denied.");
            e.printStackTrace();
        }
    }
}
