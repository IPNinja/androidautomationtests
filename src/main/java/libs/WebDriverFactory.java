//package libs;
//
//
//import com.codeborne.selenide.Configuration;
//import com.codeborne.selenide.WebDriverRunner;
//import io.github.bonigarcia.wdm.ChromeDriverManager;
//import io.github.bonigarcia.wdm.FirefoxDriverManager;
//import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
//import org.openqa.selenium.Dimension;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.logging.LogType;
//import org.openqa.selenium.logging.LoggingPreferences;
//import org.openqa.selenium.remote.CapabilityType;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.remote.RemoteWebDriver;
//import org.testng.Assert;
//
//import java.net.URL;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.logging.Level;
//
//public class WebDriverFactory {
//    public static final String ANDROID = "android";
//
//    private RemoteWebDriver driver;
//    private WebDriverWrapper driverWrapper;
//    private DesiredCapabilities capabilities;
//    private ChromeOptions chromeOptions;
//    private Dimension dimension;
//
//    public WebDriverWrapper initDriver(String seleniumGridHubURL, String browser) {
//
//        switch (browse r) {
//            case FIREFOX:
//                capabilities = setDefaultFirefoxCapabilities();
//                break;
//            case CHROME:
//                capabilities = setDefaultChromeCapabilities();
//                break;
//            case MOBILE_EMULATOR:
//                capabilities = setMobileEmulatorChromeOptions();
//                break;
//            default:
//                Assert.fail("No information about launching '" + browser + "' browser!!!");
//        }
//
//        if (seleniumGridHubURL == null || seleniumGridHubURL.equals("")) {
//            if (browser.equals(ANDROID)) {
//                ChromeDriverManager.getInstance().setup();
//                Configuration.browser = ANDROID;
//                Configuration.fastSetValue = true;
//                Configuration.versatileSetValue = true;
//                Configuration.timeout = 4000;
//                driver = new RemoteWebDriver(chromeOptions);
//            } else {
//                Assert.fail("No information about launching '" + browser + "' browser!!!");
//            }
//        } else {
//            try {
//                ReportWriter.logInfo("Start initializing RemoteWebDriver. URL: '" + seleniumGridHubURL + "'");
//                driver = new RemoteWebDriver(new URL(seleniumGridHubURL), capabilities);
//                ReportWriter.logInfo("Stop initializing RemoteWebDriver. URL: '" + seleniumGridHubURL + "'");
//            } catch (Exception e) {
//                ReportWriter.logException("Initialization RemoteWebDriver Fail. " + e.getMessage());
//                Assert.fail();
//            }
//        }
//
//        WebDriverRunner.setWebDriver(driver);
//        WebDriverRunner.getWebDriver().manage().window().setSize(dimension);
//        driverWrapper = new WebDriverWrapper(driver);
//
//        return driverWrapper;
//    }
//
//    private static DesiredCapabilities setMobileEmulatorChromeOptions() {
//        Map<String, String> mobileEmulation = new HashMap<>();
//        mobileEmulation.put("deviceName", "Galaxy S5");
//
//        LoggingPreferences logPrefs = new LoggingPreferences();
//        logPrefs.enable(LogType.BROWSER, Level.ALL);
//
//        ChromeOptions options = new ChromeOptions();
//        options.setExperimentalOption("mobileEmulation", mobileEmulation);
//        options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
//        options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//        options.setCapability(ChromeOptions.CAPABILITY, options);
//        options.setCapability("ignoreZoomSetting", true);
//        options.setCapability("enableVNC", true);
//        options.setCapability("enableVideo", true);
//
//        DesiredCapabilities caps = DesiredCapabilities.chrome();
//        caps.setCapability(ChromeOptions.CAPABILITY, options);
//        return caps;
//    }
//
//    private static DesiredCapabilities setDefaultChromeCapabilities() {
//        String downloadFileDir = System.getProperty("user.dir").replace("\\", "/") + "/";
//        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
//        chromePrefs.put("profile.default_content_settings.popups", 0);
//        chromePrefs.put("download.prompt_for_download", "false");
//        chromePrefs.put("download.default_directory", downloadFileDir);
//        chromePrefs.put("plugins.plugins_disabled", new String[]{
//                "Adobe Flash Player", "Chrome PDF Viewer"});
//
//        ChromeOptions options = new ChromeOptions();
////        options.addArguments("--start-maximized");
////        options.addArguments("user-data-dir=D:\\UserDataDir");
////        options.addArguments("-incognito");
//        options.addArguments("--no-sandbox");
//        options.addArguments("disable-infobars");
//        options.setExperimentalOption("prefs", chromePrefs);
//
//        DesiredCapabilities caps = DesiredCapabilities.chrome();
//        LoggingPreferences logPrefs = new LoggingPreferences();
//        logPrefs.enable(LogType.BROWSER, Level.ALL);
//        caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
//        caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//        caps.setCapability(ChromeOptions.CAPABILITY, options);
//        caps.setCapability("ignoreZoomSetting", true);
//        caps.setBrowserName("chrome");
//        caps.setCapability("enableVNC", true);
//        caps.setCapability("screenResolution", "1920x980x24");
//        caps.setCapability("videoScreenSize", "1280x920");
//        caps.setCapability("videoFrameRate", Integer.valueOf(24));
//        caps.setCapability("enableVideo", true);
//
//        return caps;
//    }
//
//    private static DesiredCapabilities setDefaultFirefoxCapabilities() {
//        FirefoxProfile firefoxProfile = new FirefoxProfile();
////        firefoxProfile.setEnableNativeEvents(true);
//        DesiredCapabilities firefoxCapabilities = DesiredCapabilities.firefox();
//        firefoxCapabilities.setCapability(FirefoxDriver.PROFILE, firefoxProfile);
//        firefoxCapabilities.setBrowserName("firefox");
//        firefoxCapabilities.setCapability("enableVNC", true);
//        firefoxCapabilities.setCapability("enableVideo", false);
//
//        return firefoxCapabilities;
//    }
//}
