import libs.HelperClass;
import libs.ReportWriter;
import libs.TestBase;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.ChromeBrowserPage;
import pages.MainPage;

import static io.restassured.RestAssured.get;

public class ExitNodeTests extends TestBase {

    private MainPage mainPage;
    private HelperClass helper;
    private ChromeBrowserPage chromePage;
    private static final String BASE_URL = "http://138.128.240.127:8081/rest/v1/exitnodes/";


    private void initPages(RemoteWebDriver driver) {
        ReportWriter.logTestStep("Init all pages");
        mainPage = new MainPage(driver);
        helper = new HelperClass(driver);
        chromePage = new ChromeBrowserPage(driver);
        ReportWriter.logInfo("All pages are initialized");
    }

    @Test(description = "Check license agreement popup", priority = 1)
    public void checkLicenseAgreementPopupPresenceTest() {
        ReportWriter.logInfo("Test is started");
        initPages(getDriver());
        mainPage.checkLicenseAgeementPresence();
        ReportWriter.logInfo("Test is finished");
    }

    @Test(description = "Check 'available_p' field in DB before licence accepting", dependsOnMethods = "checkLicenseAgreementPopupPresenceTest", priority = 2)
    public void checkAvailable_pParameterBeforeLicenceAcceptingTest() {
        ReportWriter.logInfo("Test is started");
        initPages(getDriver());
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(helper.executeSelectQuery(helper.biuldStringList("available_p"), "exit_node_t",
                helper.biuldStringList("affiliate_id_p='" + getAffiliateId() + "'"), "available_p",
                false, false, true),
                "");
        softAssert.assertAll();
        ReportWriter.logInfo("Test is finished");
    }

    @Test(description = "Check that 'available_p' did not changed from 0 to 1 in case licence was discard", dependsOnMethods = "checkLicenseAgreementPopupPresenceTest", priority = 3)
    public void checkAvailable_pParameterAfterDiscardingLicenceTest() {
        ReportWriter.logInfo("Test is started");
        initPages(getDriver());
        SoftAssert softAssert = new SoftAssert();
        mainPage.clickToDiscardLicenceButton();
        softAssert.assertEquals(helper.executeSelectQuery(helper.biuldStringList("available_p"), "exit_node_t",
                helper.biuldStringList("affiliate_id_p='" + getAffiliateId() + "'"), "available_p", false, false, true), "");
        softAssert.assertAll();
        ReportWriter.logInfo("Test is finished");
    }

    @Test(priority = 4, description = "Check licence agreement and link working", dependsOnMethods = "checkLicenseAgreementPopupPresenceTest")
    public void checkLicencePage() {
        ReportWriter.logInfo("Test is started");
        SoftAssert softAssert = new SoftAssert();
        initPages(getDriver());
        mainPage.checkLicencePresents();
        softAssert.assertTrue(chromePage.getLicenceHeader(), "Page is not opened");
        ReportWriter.logInfo("Test is finished");
    }

    @Test(priority = 5, description = "Check that 'available_p' value changed from 0 to 1 in case licence was accepted", dependsOnMethods = "checkLicenseAgreementPopupPresenceTest")
    public void checkAvailable_pParameterAfterLicenceAcceptingTest() {
        ReportWriter.logInfo("Test is started");
        SoftAssert softAssert = new SoftAssert();
        initPages(getDriver());
        mainPage.clickToAcceptLicenceButton();
        softAssert.assertEquals(helper.executeSelectQuery(helper.biuldStringList("available_p"), "exit_node_t",
                helper.biuldStringList("affiliate_id_p='" + getAffiliateId() + "'"), "available_p", false, false, true), "1");
        ReportWriter.logInfo("Test is finished");
    }

    @Test(priority = 6, description = "Check push requests"/*, dependsOnMethods = "checkLicenseAgreementPopupPresenceTest"*/)
    public void checkPushRequests() throws InterruptedException {
        ReportWriter.logInfo("Test is started");
        initPages(getDriver());

        ReportWriter.logTestStep("Check that entry is absent before licence accepting");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(helper.executeSelectQuery(helper.biuldStringList("available_p"), "exit_node_t",
                helper.biuldStringList("affiliate_id_p='" + getAffiliateId() + "'"), "available_p",
                false, false, true), "0",
                "Entry is present in DB");

        mainPage.clickToAcceptLicenceButton();

        ReportWriter.logTestStep("Check DB after accepting licence");
        softAssert.assertEquals(helper.executeSelectQuery(helper.biuldStringList("available_p"), "exit_node_t",
                helper.biuldStringList("affiliate_id_p='" + getAffiliateId() + "'"), "available_p",
                false, false, true), "1",
                "'available_p' is still has '0' value");

        ReportWriter.logTestStep("Get 'name_id_p' field value from DB");
        String nameIdPField = helper.executeSelectQuery(helper.biuldStringList("name_id_p"), "exit_node_t",
                helper.biuldStringList("affiliate_id_p='" + getAffiliateId() + "'"), "name_id_p",
                false, false, true);
        ReportWriter.logInfo("'name_id_p': " + nameIdPField);

        ReportWriter.logTestStep("Get 'partner_software_name_p' field value from DB");
        String partnerSoftwareName = helper.executeSelectQuery(helper.biuldStringList("partner_software_name_p"), "exit_node_t",
                helper.biuldStringList("affiliate_id_p='" + getAffiliateId() + "'"), "partner_software_name_p",
                false, false, true);
        ReportWriter.logInfo("'partner_software_name_p': " + partnerSoftwareName);

        ReportWriter.logTestStep("Close app");
        helper.closeApp();

        Thread.sleep(2000);

        ReportWriter.logTestStep("Check 'available_p' after app closing");
        softAssert.assertEquals(helper.getAvailable_pFromDb(helper.biuldStringList("name_id_p='" + nameIdPField + "'",
                "partner_software_name_p='" + partnerSoftwareName + "'"), 3), "0",
                "Value did not changed when app was closed");

        ReportWriter.logTestStep("Send push request to: " + BASE_URL + nameIdPField + "/sendPush/" + partnerSoftwareName);
        get(BASE_URL + nameIdPField + "/sendPush/" + partnerSoftwareName).then().statusCode(200);

        Thread.sleep(2000);

        ReportWriter.logTestStep("Check 'available_p' after push request");
        softAssert.assertEquals(helper.executeSelectQuery(helper.biuldStringList("available_p"), "exit_node_t",
                helper.biuldStringList("affiliate_id_p='" + getAffiliateId() + "'"), "available_p",
                false, false, true), "1",
                "'available_p' did not changing to '1' after push request");

        softAssert.assertAll();
        ReportWriter.logInfo("Test is finished");
    }
}
